'use strict';

const cat = {
    name: 'Fluffy',
    color: 'White'
}
display(cat['color']);

// using non-canonical property name
cat['Eye Color'] = 'Green';
display(cat['Eye Color']);

display(cat);
display(Object.getOwnPropertyDescriptor(cat, 'name'));

// making property read-only
Object.defineProperty(cat, 'name', {writable: false});
display(Object.getOwnPropertyDescriptor(cat, 'name'));
// cat.name = 'Scratchy'; // raise an error: "name" is read-only

const cat2 = {
    name: { first: 'Fluffy', last: 'LaBeouf' },
    color: 'White'
}
display(cat2.name);
// this does not work on objects, it only prevents from change object reference, not it's content
Object.defineProperty(cat2, 'name', {writable: false});
cat2.name.first = 'Scratchy';
display(cat2.name);

// make object read-only
Object.freeze(cat2.name);
// cat2.name.first = 'Tom'; // raise an error

// using enumerable attributes
for (let propertyName in cat2) {
    display(propertyName + ': ' + cat2[propertyName]);
}

// if enumerable is false, property will not be used in loops
Object.defineProperty(cat2, 'name', {enumerable: false});
for (let propertyName in cat2) {
    display(propertyName + ': ' + cat2[propertyName]);
}
// if enumerable is false, property will not be shown in object keys
display(Object.keys(cat2));

Object.defineProperty(cat2, 'name', {enumerable: true});
display(Object.keys(cat2));

display(JSON.stringify(cat2));

// if enumerable is false, property will not be serialized in JSON
Object.defineProperty(cat2, 'name', {enumerable: false});
display(JSON.stringify(cat2));

// if enumerable is false, property can still be accessed with bracket notation
display(cat2['name']);
Object.defineProperty(cat2, 'name', {enumerable: true});

// using configurable attribute
Object.defineProperty(cat2, 'name', {configurable: false});
// if configurable is false, object's enumerable attribute cannot be changed
// Object.defineProperty(cat2, 'name', {enumerable: false}); // raise an error

// if configurable is false, object's configurable attribute cannot be changed
// Object.defineProperty(cat2, 'name', {configurable: true}); // raise an error

// if property is configurable false, it cannot be deleted
// delete cat2.name // raise an error

// but can change property writable attribute
Object.defineProperty(cat2, 'name', {writable: false});

// using getters and setters
const cat3 = {
    name: { first: 'Fluffy', last: 'LaBeouf' },
    color: 'White'
}

Object.defineProperty(cat3, 'fullName',
    {
        get: function() {
            return this.name.first + ' ' + this.name.last;
        },
        set: function(value) {
            const nameParts = value.split(' ');
            this.name.first = nameParts[0];
            this.name.last = nameParts[1];
        }
    });

display(cat3.fullName);

cat3.fullName = 'Muffin Top'
display(cat3.fullName);
display(cat3.name);
