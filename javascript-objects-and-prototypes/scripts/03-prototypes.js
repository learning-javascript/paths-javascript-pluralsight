'use strict';

const arr = ['red', 'blue', 'green'];

const last = arr[arr.length - 1];
display(last);

// adding property 'last' to an array to access last element
Object.defineProperty(arr, 'last', {get: function() {
    return this[this.length - 1];
}});
display(arr.last);

// but new array doesn't have this property
const arr2 = ['one', 'two', 'three'];
display('arr2.last = ' + arr2.last);

// define new attribute to Array.prototype, so it will be available for all arrays
Object.defineProperty(Array.prototype, 'last', {get: function() {
    return this[this.length - 1];
}});
display(arr.last);
display(arr2.last);

// Array is just a function
display(Array);

// array can be constracted using Array constructor function
const arr3 = new Array('red', 'blue', 'green');
display(arr3);

// prototype is an object which exist on every function in JavaScript
const myFunc = function() {}
display(myFunc.prototype);

// javascript objects doesn't have prototype property
const cat = { name: 'Fluffy' }
display('cat.prototype = ' + cat.prototype);

// object have __proto__ property
display(cat.__proto__);

function Cat(name, color) {
    this.name = name;
    this.color = color;
}
display(Cat.prototype);

const fluffy = new Cat('Fluffy', 'White');
display(fluffy.__proto__);
// Cat.prototype and fluffy.__proto__ are pointing to same prototype instance
display(Cat.prototype === fluffy.__proto__);

Cat.prototype.age = 3;
display(Cat.prototype);
display(fluffy.__proto__);

const muffin = new Cat('Muffin', 'Brown');
display(muffin.__proto__);

// instance vs prototype properties
display(fluffy.age);
display(muffin.age);

// this changes property of an instance, but not a property of prototype
fluffy.age = 5;
display(fluffy.age);
display(fluffy.__proto__.age);
display(muffin.age);

// age property can be accessed from the object, but it is not a property of this object
display(Object.keys(muffin));
display(muffin.hasOwnProperty('age'));
display(muffin.hasOwnProperty('color'));

// changing function's prototype
Cat.prototype = {age: 6};
display(fluffy.age);
display(muffin.age);
display(Cat.prototype.age);

const snowbell = new Cat('Snowbell', 'White');
display(snowbell.age);

// multiple level of inheritance
display(fluffy.__proto__);
display(fluffy.__proto__.__proto__);
// display(fluffy.__proto__.__proto__.__proto__); // the last __proto__ is null

// creating own prototypal inheritance
function Animal() {}
Animal.prototype.speak = function() {
    display('Grunt');
}
Cat.prototype = Object.create(Animal.prototype);
const fluffy2 = new Cat('Fluffy', 'White');
fluffy2.speak();

function Animal2(voice) {
    this.voice = voice || 'grunt';
}
Animal2.prototype.speak = function() {
    display(this.voice);
}
function Cat2(name, color) {
    Animal2.call(this);
    this.name = name;
    this.color = color;
}
Cat2.prototype = Object.create(Animal2.prototype);
const fluffy3 = new Cat2('Fluffy', 'White');
fluffy3.speak();

// there is 3 lines important when creating prototypal inheritance
function Cat3(name, color) {
    Animal2.call(this, 'Meow'); // line 1
    this.name = name;
    this.color = color;
}
Cat3.prototype = Object.create(Animal2.prototype); // line 2
const fluffy4 = new Cat3('Fluffy', 'White');
fluffy4.speak();
display(fluffy4);
display(fluffy4 instanceof Cat3);
display(fluffy4 instanceof Animal2);

Cat3.prototype.constructor = Cat3; // line 3
const fluffy5 = new Cat3('Fluffy', 'White');
display(fluffy5);
display(fluffy5.__proto__);
display(fluffy5.__proto__.__proto__);

// creating prototypes with classes
class Animal3 {
    constructor(voice) {
        this.voice = voice || 'grunt';
    }

    speak() {
        display(this.voice);
    }
    /* this speak function replaces the following code
Animal2.prototype.speak = function() {
    display(this.voice);
}
    */
}
/* the code above replaces the code below
function Animal2(voice) {
    this.voice = voice || 'grunt';
}
*/

class Cat4 extends Animal3 {
    constructor(name, color) {
        super('Meow');
        this.name = name;
        this.color = color;
    }
}
/* this class Cat4 code replaces the following code
function Cat3(name, color) {
    Animal2.call(this, 'Meow');
    this.name = name;
    this.color = color;
}
Cat3.prototype = Object.create(Animal2.prototype);
Cat3.prototype.constructor = Cat3;
*/
const fluffy6 = new Cat4('Fluffy', 'White');
fluffy6.speak();
display(fluffy6);
display(fluffy6.constructor);
display(Object.keys(fluffy6.__proto__.__proto__));
display(fluffy6.__proto__.__proto__.hasOwnProperty('speak'));
