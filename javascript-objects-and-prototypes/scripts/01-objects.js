'use strict';

// creating JavaScript object with object literal
const cat = {name: 'Fluffy', color: 'White'};
display(cat.name);

// dynamically adding properties to object
cat.age = 3;
display(cat.age);

// adding methods
cat.speak = function() {
    display('Meeooow');
}
cat.speak();

// creating JvaScript object with constructor function
function Cat(name, color) {
    this.name = name;
    this.color = color;
}

const cat2 = new Cat('Fluffy', 'White');
display(cat2);

// creating javascript object with Object.create() function
const cat3 = Object.create(Object.prototype,
{
    name: {
        value: 'Fluffy',
        enumerable: true,
        writable: true,
        configurable: true
    },
    color: {
        value: 'White',
        enumerable: true,
        writable: true,
        configurable: true
    }
});
display(cat3);

// using ES6 classes
class Cat4 {
    constructor(name, color) {
        this.name = name;
        this.color = color;
    }

    speak() {
        display('Meeooow');
    }
}
const cat4 = new Cat4('Fluffy', 'White');
display(cat4);
cat4.speak();
