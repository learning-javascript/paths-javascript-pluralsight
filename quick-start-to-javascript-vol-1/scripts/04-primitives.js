console.log('this is a string');
console.log('typeof:', typeof 'this is a string');

console.log('2 + 2 =', 2 + 2);
console.log('typeof:', typeof (2 + 2));

console.log('2 + "2" =', 2 + '2');
console.log('typeof:', typeof (2 + '2'));

console.log('Hello' + 'world');
console.log('Hello' + ' ' + 'world');

console.log('10 > 2', 10 > 2);
console.log('typeof (10 > 2)', typeof (10 > 2));
console.log('10 < 2', 10 < 2);
console.log('typeof (10 < 2)', typeof (10 < 2));

/* 
Game
Try to guess the type of each expression's result:
    300 / 2;
    "Hello" + "World";
    4 < 2;
    a + b;
    3 + "6";
*/

console.log('300 / 2', 'number');
console.log('typeof 300 / 2', typeof (300 / 2));

console.log('"Hello" + "World"', 'string');
console.log('typeof ("Hello" + "World")', typeof ('Hello' + 'World'));

console.log('4 < 2', 'boolean');
console.log('typeof (4 < 2)', typeof (4 < 2));

console.log('a + b', 'is not defined');
//console.log('typeof (a + b)', typeof (a + b)); // this line causes error

console.log('3 + "6"', 'string');
console.log('typeof (3 + "6"', typeof (3 + '6'));