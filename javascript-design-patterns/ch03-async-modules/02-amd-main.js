require(['jquery', './02-amd-api'], function ($, api) {
    $(document).ready(function () {
        $('.load-tweets').submit(function (e) {
            e.preventDefault();

            const user = $(this).find('input').val();

            api.timeline(user).done(function (tweets) {
                const el = $('.twitter').empty();
                const lis = [];

                for (let i = 0, il = tweets.results.length; i < il; i++) {
                    const name = tweets.results[i].title ? tweets.results[i].title : tweets.results[i].name;
                    lis.push($(`<li>${name}</li>`));
                }

                el.append(lis);
            });
        });
    });
});
