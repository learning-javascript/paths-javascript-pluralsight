define(['jquery'], function ($) {
    return {
        timeline: function (user) {
            return $.getJSON('https://swapi.co/api/' + user);
        }
    };
});
