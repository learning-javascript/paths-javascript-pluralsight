const cluster = require('cluster');

if (cluster.isMaster) {
    const cpuCount = require('os').cpus().length;

    for (let i = 0; i < cpuCount; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker) => {
        console.log(`Worker ${worker.id} died. Restarting...`);
        cluster.fork();
    });
} else {
    const express = require('express');
    const app = express();
    const path = require('path');

    app.get('/home/date', (req, res) => {
        console.log(`Request to worker ${cluster.worker.id}`);
        const date = new Date();
        const delay = Math.round(Math.random() * 15 + 5) * 1000;
        setTimeout(() => {
            res.send(`${new Date()} - ${date}`);
        }, delay);
    });

    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname + '/02-index.html'));
    });

    app.get('/home', (req, res) => {
        res.sendFile(path.join(__dirname + '/02-index-recursive-setTimeout.html'));
    });

    app.listen(3000, () => {
        console.log('Server listening on port 3000');
    });
}