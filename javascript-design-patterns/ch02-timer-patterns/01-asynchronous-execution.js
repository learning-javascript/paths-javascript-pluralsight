const buffer = function (items, iterFn, callback) {
    let i = 0;
    const len = items.length;
    setTimeout(function () {
        let result;
        for (const start = +new Date(); i < len && result !== false && ((+new Date()) - start < 50); i++) {
            result = iterFn.call(items[i], items[i], i);
        }

        if (i < len && result !== false) {
            setTimeout(arguments.callee, 20);
        } else {
            callback(items);
        }
    }, 20);
};

const demo = function (result) {
    const html = [];
    buffer(result, function (item) {
        html.push(`<li>${item}</li>`);
    }, function () {
        console.log(html);
    });
};

const arr = [];
for (let i = 0; i < 1000; i++) {
    arr.push(i);
}
demo(arr);
