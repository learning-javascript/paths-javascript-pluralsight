require(['jquery', './02-big-cart', './02-mini-cart', './02-product-list'], function ($, bigCart, miniCart, productList) {
    $(document).ready(function () {
        bigCart.init();
        miniCart.init();
        productList.init();
    });
});
