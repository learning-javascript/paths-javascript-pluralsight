define(['./02-pubsub', 'jquery'], function (pubsub, $) {
    let cart;
    let count = 0;

    pubsub.sub('add-to-cart', function () {
        count++;

        cart.find('span').html(count);
    });

    pubsub.sub('remove-from-cart', function () {
        count--;

        cart.find('span').html(count);
    });

    return {
        init: function () {
            cart = $('.mini-cart');
        }
    };
});
