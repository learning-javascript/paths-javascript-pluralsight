require(['./02-jquery-promise-api', 'jquery'], function (api, $) {
    $.fn.blinky = function (args) {
        const opts = {frequency: 1000, count: -1};
        args = $.extend(true, opts, args);

        let i = 0;
        const that = this;
        const dfd = $.Deferred();

        function go() {
            if (that.length === 0) {
                return dfd.reject();
            }
            if (i === args.count) {
                return dfd.resolve();
            }
            i++;
            $(that).fadeOut(args.frequency / 2).fadeIn(args.frequency / 2);
            setTimeout(go, args.frequency);
        }

        go();

        return dfd.promise();
    };

    $(document).ready(function () {
        $('.load-tweets').submit(function (e) {
            e.preventDefault();
            const search = $(this).find('input').val();
            $.when(api.timeline(search), $(this).find('input').blinky({count: 2}))
                .done(function (args) {
                    const el = $('.twitter').empty();
                    const lis = [];
                    const data = args[0];

                    for (let i = 0; i < data.results.length; i++) {
                        const name = data.results[i].title ? data.results[i].title : data.results[i].name;
                        lis.push($(`<li>${name}</li>`));
                    }

                    el.append(lis);
                }).fail(function () {
                    alert('Something has gone wrong!');
                });
        })
    });
});