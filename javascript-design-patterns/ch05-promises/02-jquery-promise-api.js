define(['jquery'], function ($) {
    return {
        timeline: function (search) {
            return $.getJSON('https://swapi.co/api/' + search);
        }
    };
});
