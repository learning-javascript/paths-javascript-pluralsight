const Book = function (name, price) {
    const priceChanging = [];
    const priceChanged = [];
    this.name = function (val) {
        return name;
    };
    this.price = function (val) {
        if (val !== undefined &&  val !== price) {
            for (let i = 0; i < priceChanging.length; i++) {
                if (!priceChanging[i](this, val)) {
                    return price;
                }
            }
            price = val;
            for (let i = 0; i < priceChanged.length; i++) {
                priceChanged[i](this);
            }
        }
        return price;
    };
    this.onPriceChanging = function (callback) {
        priceChanging.push(callback);
    };
    this.onPriceChanged = function (callback) {
        priceChanged.push(callback);
    };
};

const book = new Book('Javascript: The Good Parts', 23.99);

console.log('The name is:', book.name());
console.log(`The price is: \$${book.price()}`);

book.onPriceChanging(function (b, price) {
    if (price > 100) {
        console.log('System error, price has gone unexpectedly high');
        return false;
    }
    return true;
});

book.price(19.99);
console.log(`The new price 1 is: \$${book.price()}`);

book.price(200);
console.log(`The new price 2 is: \$${book.price()}`);
