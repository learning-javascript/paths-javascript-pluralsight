const friends = ['Mark', 'Lisa', 'Danny', 'Peter', 'Steven', 'Claudette'];

function greetFriends() {
    for (let i = 0; i < friends.length; i++) {
        console.log('Oh hi', friends[i]);
    }
}

greetFriends();