let i = 0;

console.log('do...while loop');
do {
    console.log(i);
    i++;
} while (i < 11);

i = 0;
console.log('while loop');
while (i < 11) {
    console.log(i);
    i++;
}

let sad = confirm('Would you like to be cheered up?');

while (sad) {
    alert('What do you call a boomerang that does not come back? ... A stick!');

    const yes = confirm('Would you like to continue our session?');

    if (yes) {
        const response = prompt('Tell me your troubles:', 'Type your feelings here');

        if (response) {
            alert('I am sorry you are feeling down about that.');
            sad = false;
        }
    } else {
        sad = false;
    }
}