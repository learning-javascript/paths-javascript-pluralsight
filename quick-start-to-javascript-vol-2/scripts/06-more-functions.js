function sayHello(name) {
    console.log('Oh hi', name);
}

sayHello('Mark');
sayHello('Lisa');
sayHello('Danny');

sayHello();

function calculateArea(side) {
    return side * side;
}

console.log(calculateArea(5));
console.log(calculateArea(987654321));

function calculateArea2(sideA, sideB) {
    return sideA * sideB;
}

console.log(calculateArea2(5, 6));

function randomNumber() {
    return Math.random();
}

console.log(randomNumber());