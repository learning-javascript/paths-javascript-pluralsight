const friends = ['Mark', 'Lisa', 'Danny'];

console.log(friends);
console.log(friends[0]);
console.log(friends[1]);
console.log(friends[2]);

friends[3] = 'Bob';
console.log(friends);

friends.push('Mary');
console.log(friends);
