const Task = function (name) {
    this.name = name;
    this.completed = false;
};
Task.prototype.complete = function () {
    console.log('completing task:', this.name);
    this.completed = true;
};
Task.prototype.save = function () {
    console.log('saving task:', this.name);
};
Task.prototype.toString = function () {
    return this.name + ' is completed: ' + this.completed;
};

const myTask = new Task('Legacy Task');
myTask.complete();
myTask.save();
console.log('Task:', myTask.toString());

const urgentTask = new Task('Urgent Task');
// decorating Task with new property
urgentTask.priority = 2;
// decorating Task with new function
urgentTask.notify = function () {
    console.log('notifying important people');
};
urgentTask.complete();
// overriding Task function
urgentTask.save = function () {
    this.notify();
    Task.prototype.save.call(this);
};
urgentTask.save();
console.log('Task:', urgentTask.toString());
