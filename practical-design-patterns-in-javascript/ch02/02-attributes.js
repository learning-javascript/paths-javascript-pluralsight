// assigning keys and values

// using dot notation
const obj = {};
obj.param = 'new value';
console.log(obj.param);

// using square bracket notation
const obj2 = {};
obj2['param'] = 'new value 2';
console.log(obj2['param']);

// advantage of using bracket notation is ability to use variable as attribute key
const obj3 = {};
const val = 'value';
obj3[val] = 'new value 3';
console.log(obj3[val]);