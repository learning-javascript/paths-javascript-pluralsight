const task = {
    title: 'My task',
    description: 'My Description'
};
task.toString = function () {
    return this.title + ' ' + this.description;
};
console.log(task.title);
console.log(task.toString());

const task2 = Object.create(Object.prototype);
task2.title = 'My task 2';
task2.description = 'My Description 2';
task2.toString = function () {
    return this.title + ' ' + this.description;
};
console.log(task2.title);
console.log(task2.toString());

const task3 = new Object();
task3.title = 'My task 3';
task3.description = 'My Description 3';
task3.toString = function () {
    return this.title + ' ' + this.description;
};
console.log(task3.title);
console.log(task3.toString());
