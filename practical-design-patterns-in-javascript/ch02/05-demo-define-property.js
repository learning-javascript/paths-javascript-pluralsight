const task = {
    title: 'My task',
    description: 'My Description'
};

Object.defineProperty(task, 'toString', {
    value: function () {
        return this.title + ' ' + this.description;
    },
    writable: false,
    enumerable: true,
    configurable: true
});

console.log(task.toString());

// if writable is true, following code will override toString() function
// but with writable = false, this will fail
task.toString = 'hi'; // this fails silently
console.log(task.toString());

// enumerable
console.log(task);
console.log(Object.keys(task));

Object.defineProperty(task, 'toString', {enumerable: false});
console.log(Object.keys(task));
