// create object using object literal
const obj = {};

// create object using Object.create() function
const nextObj = Object.create(Object.prototype);

// create object with new keyword
const lastObj = new Object();
