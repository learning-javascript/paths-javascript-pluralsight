const taskHandler = require('./10-node-singleton-taskHandler');
// this will create singleton
const myrepo = require('./10-node-singleton');
// this will create new instance
// const repo = require('./10-node-singleton');
// const myrepo = repo();

myrepo.save('fromMain');
myrepo.save('fromMain');
myrepo.save('fromMain');
taskHandler.save();
taskHandler.save();
taskHandler.save();
taskHandler.save();
