const Repo = require('./07-task-repository');

const Task = function (data) {
    this.name = data.name;
    this.completed = false;
};
Task.prototype.complete = function () {
    console.log('completing task:', this.name);
    this.completed = true;
};
Task.prototype.save = function () {
    console.log('Saving task:', this.name);
    Repo.save(this);
};
Task.prototype.toString = function () {
    return this.name + ' is completed: ' + this.completed;
};

module.exports = Task;
