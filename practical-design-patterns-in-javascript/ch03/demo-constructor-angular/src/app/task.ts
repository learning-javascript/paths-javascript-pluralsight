export class Task {
  name: string;
  completed: boolean;

  constructor(name: string, completed?: boolean) {
    this.name = name;
    this.completed = completed || false;
  }

  complete() {
    console.log('completing task: ', this.name);
    this.completed = true;
  }

  save() {
    console.log('saving task:', this.name);
  }

  toString() {
    return this.name + ' is completed: ' + this.completed;
  }
}
