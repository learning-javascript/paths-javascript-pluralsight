const Task = require('./07-task');
const Repo = require('./07-task-repository');

const task1 = new Task(Repo.get(1));

const task2 = new Task({name: 'creating demo for modules'});
const task3 = new Task({name: 'creating demo for singletons'});
const task4 = new Task({name: 'creating demo for prototypes'});

task1.complete();
task2.complete();
task3.save();
task4.save();

console.log('task 1:', task1.toString());
console.log('task 2:', task2.toString());
console.log('task 3:', task3.toString());
console.log('task 4:', task4.toString());
