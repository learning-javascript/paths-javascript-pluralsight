const Module = function() {
    const privateVar = 'I am private...';
    return {
        method: function() {
            // do something
        },
        nextMethod: function () {
            // do something else
        }
    };
};
