// this will create singleton
const myrepo = require('./10-node-singleton');
// this will create new instance
// const repo = require('./10-node-singleton');
// const myrepo = repo();

const taskHandler = function () {
    return {
        save: function () {
            myrepo.save('Hi from taskHandler');
        }
    };
};
module.exports = taskHandler();
