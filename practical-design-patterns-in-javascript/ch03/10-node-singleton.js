const repo = function () {
    let called = 0;

    const save = function (task) {
        called++;
        console.log('Saving ' + task + ' Called ' + called + ' times');
    };
    console.log('newing up task repo');
    return {
        save
    };
};
// this will not create singleton
//module.exports = repo;
// this will create singleton, since node will cache this for every require calls
module.exports = repo();
