const repoFactory = function () {
    const repos = this;
    const repoList = [
        {name: 'task', source: ''},
        {name: 'user', source: ''},
        {name: 'project', source: ''}
    ];
    repoList.forEach(function (repo) {
        repos[repo.nanm] = require(repo.source)();
    })
};

module.exports = new repoFactory;
