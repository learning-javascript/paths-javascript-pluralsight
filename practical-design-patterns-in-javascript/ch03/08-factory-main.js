const Task = require('./07-task');
const repoFactory = require('./08-factory');

const task1 = new Task(repoFactory.task.get(1));
const task2 = new Task(repoFactory.task.get(2));

const user = new Task(repoFactory.user.get(1));
const project = new Task(repoFactory.project.get(1));

task1.user = user;
task1.project = project;

task1.save();
