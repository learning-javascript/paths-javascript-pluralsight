const Task = function (name) {
    this.name = name;
    this.completed = false;
};
Task.prototype.complete = function () {
    console.log('completing task:', this.name);
    this.completed = true;
};
Task.prototype.save = function () {
    console.log('Saving task:', this.name);
};
Task.prototype.toString = function () {
    return this.name + ' is completed: ' + this.completed;
};

// functions assigned to prototype are not copied, but there is only 1 copy of each function
const task1 = new Task('creating demo for constructors');
const task2 = new Task('creating demo for modules');
const task3 = new Task('creating demo for singletons');
const task4 = new Task('creating demo for prototypes');

task1.complete();
task2.save();
task3.save();
task4.save();

console.log('task 1:', task1.toString());
console.log('task 2:', task2.toString());
console.log('task 3:', task3.toString());
console.log('task 4:', task4.toString());
