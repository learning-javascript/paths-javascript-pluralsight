'use strict';

class Task {
    constructor(name) {
        this.name = name;
        this.completed = false;
    }

    complete() {
        console.log('completing task:', this.name);
        this.completed = true;
    }

    save() {
        console.log('Saving task:', this.name);
    }

    toString() {
        return this.name + ' is completed: ' + this.completed;
    }
}

const task1 = new Task('creating demo for constructors');
const task2 = new Task('creating demo for modules');
const task3 = new Task('creating demo for singletons');
const task4 = new Task('creating demo for prototypes');

task1.complete();
task2.save();
task3.save();
task4.save();

console.log('task 1:', task1.toString());
console.log('task 2:', task2.toString());
console.log('task 3:', task3.toString());
console.log('task 4:', task4.toString());
