function asyncMethod(message, cb) {
    setTimeout(function () {
        console.log(message);
        cb();
    }, 500);
}

// below is a christmas tree code
asyncMethod('Open DB Connection', function () {
    asyncMethod('Find User', function () {
        asyncMethod('Validate User', function () {
            asyncMethod('do stuff', function () {});
        });
    });
});

