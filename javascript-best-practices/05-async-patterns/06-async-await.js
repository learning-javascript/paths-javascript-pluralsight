'use strict';

function asyncMethod(message, num) {
    return new Promise(function (fulfill, reject) {
        setTimeout(function () {
            console.log(message + ' ' + num);
            fulfill(num + 1);
        }, 500);
    });
}

// async + await works in newest browser

async function main() {
    const one = await asyncMethod('Open DB Connection', 0);
    const two = await asyncMethod('Find User', one);
    const three = await asyncMethod('Validate User', two);
    await asyncMethod('do stuff', three);
}

main();