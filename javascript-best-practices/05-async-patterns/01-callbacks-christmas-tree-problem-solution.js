var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongodb = require('mongodb').MongoClient;

/*
Recommendation on how to deal with callbacks to avoid christmas tree effect
1. create named function and use them as callback arguments
2. most callbacks are called with parameters like (error, result), remember to handle error
3. return your callbacks
 */

// use named functions instead of anonymous, this way can avoid christmas tree effect
function findAndValidateUser(username, password, done) {
    var url = 'mongodb://localhost:27017/libraryApp';

    function validateUser(err, results) {
        // remember to handle errors in callback
        if (err) {
            // it is good idea to return, whenever you execute callback inside a function
            return done(err, null);
        }
        if (results.password === password) {
            var user = results;
            return done(null, user);
        } else {
            return done(null, false, {
                message: 'Bad password'
            });
        }
    }

    function findUser(err, db) {
        if (err) {
            return done(err, null);
        }
        var collection = db.collection('users');
        collection.findOne({
            username: username
        }, validateUser);
    }

    mongodb.connect(url, findUser);
}

module.exports = function () {
    passport.use(new LocalStrategy({
        _usernameField: 'username',
        _passwordField: 'password'
    }, findAndValidateUser));
};
