'use strict';

var obj = {a: 100, b: 200};
var myVar = 10;

console.log(obj);
delete obj.a;
console.log(obj);

console.log(myVar);
// in none strict mode following line will fail silently and do nothing
// in strict come this line will generate error
delete myVar;
