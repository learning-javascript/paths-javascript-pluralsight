
var obj = function () {
    var _this = this;
    console.log(this);
    this.hello = 'hello';
    _this.hello2 = 'hello';

    /*
    // this code doesn't work
    return {
        greet: function () {
            console.log(this.hello);
        }
    };
    */

    this.greet = function () {
        console.log(this.hello);
    };

    _this.greet2 = function () {
        console.log(_this.hello2);
    };

    this.delayGreeting = function () {
        setTimeout(this.greet, 1000);
    };

    this.delayGreeting1 = function () {
        setTimeout(this.greet.bind(this), 1000);
    };

    this.delayGreeting2 = function () {
        setTimeout(_this.greet2, 1000);
    };

    // this is returned implicitly
    // return this;
};

// this doesn't work
//obj().greet();

var greeter1 = obj();
var greeter = new obj();
console.log(greeter);
greeter.greet();

greeter.delayGreeting();
greeter.delayGreeting1();
greeter.delayGreeting2();
