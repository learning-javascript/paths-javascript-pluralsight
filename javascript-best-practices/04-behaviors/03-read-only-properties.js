'use strict';

var obj = {};

Object.defineProperty(obj, 'readOnly', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: 'This var is read only'
});

// in not strict mode the following line will do nothing, it will not reassign value nor cause an error
// it will fail silently
// in strict mode this line will raise an error
obj.readOnly = 'I wrote this';
console.log(obj.readOnly);
