'use strict';

var obj = {
    val: 'Hi there',
    printVal: function() {
        console.log(this);
        console.log(this.val);
    }
};

obj.printVal();

var obj2 = {
    val: 'Whats up'
};
obj2.printVal = obj.printVal;

obj2.printVal();

var print = obj.printVal;
// in no strict mode this will be bound to global scope
// in strict mode this will raise an error, because this will be undefined
//print();

// it is possible to bind this context to the function
var print2 = obj.printVal.bind(obj2);
print2();
