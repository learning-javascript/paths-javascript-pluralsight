var toPrint = 'print me';

function print(out) {
    var stringToPrint = out;
    console.log(stringToPrint);
    // because toPrint is defined in the parent scope, it is visible inside the function
    console.log(toPrint);
}

print('Hello');
// the following line will cause is not defined error, because variable is not visible out side
// of it's scope. In this case out side of the function print
// console.log(stringToPrint);

function print2(out) {
    // this variable is defined into global scope
    stringToPrint2 = out;
    console.log(stringToPrint2);
}
// stringToPoint2 is not defined yet
// console.log(stringToPrint2);
print2('Hello 2');
// but here it is defined and is visible out side of the function, since it is define in global scope
console.log(stringToPrint2);
