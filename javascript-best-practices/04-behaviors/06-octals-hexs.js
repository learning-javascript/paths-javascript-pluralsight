'use strict';

// in no strict mode this will not fail
var x = 120;
var y = 012;
// in no strict mode this number will be converted to decimal number
// in strict mode this will cause an error
var z = 002;

console.log(x + y + z);

var a = 0x12;
console.log(x + a + z);
