'use strict';

// in no strict mode this will be accepted
// in strict mode this will cause an error
function x(a, b, a) {
    console.log(a);
}

// and this line will print 3
x(1, 2, 3);
