// JavaScript is trying to help... Don't let it :)

// even though it will be tempting to use this in global scope, the might cause a problems,
// because it will ba applied to everything, including 3rd party packages
// 'use strict';

var toPrint = 'print me';

function print(out) {
    // instead it is better to use this inside the local scope
    'use strict';
    // the next line will cause is not defined error
    // stringToPrint = out;
    var stringToPrint = out;
    console.log(stringToPrint);
}

randomVariable = 42;
print('Hello');
