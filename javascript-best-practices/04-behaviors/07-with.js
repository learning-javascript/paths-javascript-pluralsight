
var obj = {
    a: {
        b: {
            c: 'hello'
        }
    }
};

console.log(obj.a.b.c);

var c = 'this is important';

with (obj.a.b) {
    console.log(c);
}

// with is deprecated in JavaScript, it should not be used
// in strict mode it causes an error

// same can be done cleaner way
(function (newVar) {
    console.log(newVar);
})(obj.a.b.c);
