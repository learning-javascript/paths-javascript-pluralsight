# Recommendations for production code

## Some useful configs for NPM

    npm config set save=true
    
This command sets automatic save when do `install` new package.
This way `--save` options is not necessary.


    npm config set save-exact=true

this command will tell npm to install exact version of the package.

## Environment variables

If we have following code in our application:

    console.log('Express listening on port ' + process.env.PORT);

And we've defined following `npm` `scripts`:

    "start": "node app.js"
    
When we would run it without setting environment variable `PORT`, port will be `undefined`:

    $ npm start
    ...
    Express listening on port undefined

`foreman` package provides some defaults for environment variables.
It can be installed either globally (with `-g` option) or in project like this:

    npm install -S foreman

If it is installed globally it can be run as command line tool:

    nf start
    
Or if it is installed for project only, it can be configured as custom npm `script`:

    "nf": "nf start"

and run as following:

    $ nf start
    ...
    Express listening on port 5000

`foreman` will look for environment file and if it wouldn't find one, it will use defaults.
But we can configure project environment variables in `.env` file:

    {
      "port": 9000,
      "connection": {
        "sql": "",
        "mongo": ""
      }
    }

When running command again, `foreman` will pick up our configurations:

    $ nf start
    ...
    Express listening on port 9000
    
## Cross Platform Concerns

If for example our project have script file `myobject.js`,
but it is referenced in the code using mixed case, like following:

    var myObject = require('./myObject');

It will work in Windows and Mac, but it would not work in Linux.

Recommendation for script names is always use lower case with `-` as word separator.

In example above file should be named `my-script.js` and then it can be referenced in the code
like following:

    const myObject = require('./my-object');
    
This will work in all environments.
