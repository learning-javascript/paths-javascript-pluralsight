// this will work in Windows and Mac but wouldn't in Linux, because file is all lower case
const myObject = require('./myObject');
// this will work in all environments
const myObject2 = require('./my-object');

console.log('Express listening on port ' + process.env.PORT);

console.log(myObject.hi);
console.log(myObject2.hi);