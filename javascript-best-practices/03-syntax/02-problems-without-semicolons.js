var a = 12      // rule 1a
var b = 13      // rule 1a
// next line does not got semicolon inserted automatically
// and [ is counted as part of same expression, what leads to error:
//      Cannot read property 'forEach' of undefined
var c = a + b

['menu', 'items', 'listed']
    .forEach(function (element) {
        console.log(element)    // rule 1b
    })