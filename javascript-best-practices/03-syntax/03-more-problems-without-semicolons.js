var a = 12
var b = 13
// next line will not get semicolon and a will be treated as function
var c = b + a

(function () {
    console.log('inside my life');
    console.log('doing secret stuff...')
})