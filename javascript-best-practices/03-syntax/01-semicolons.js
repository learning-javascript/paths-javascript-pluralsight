/* Automatic semicolon insertion
"When, as a Script or Module is parsed from left to right, a token (called the offending token)
is encountered that is not allowed by any production of the grammar,"
EcmaScript Standards
*/

/*
Rule 1a:
"The offending token is separated from the previous token by at least one LineTerminator."
EcmaScript Standards
 */
var a = 12              // rule 1a => var a = 12;
var b = 13              // rule 1a => var b = 13;

/*
Rule 1b:
"The offending token is ] (curly brace)
EcmaScript Standards
 */
if(a){console.log(a)}   // rule 1b => if(a){console.log(a);}

/*
Rule 2:
"When, as the Script or Module is parsed from left to right, the end of the input stream of token
is encountered, then a semicolon is automatically inserted at the end of the input stream."
EcmaScript Standards
 */
// line below, since it is end of the file is meeting rule 2
// rule 2 => console.log(a+b);
console.log(a+b)