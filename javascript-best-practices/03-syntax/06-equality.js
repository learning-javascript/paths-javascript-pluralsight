// double equals (==) do type conversion to make elements on both side of the same time
let x = 1;
let y = '1';

if (x == y) {
    console.log('1) Equals');
} else {
    console.log('1) Not equals');
}

y = true;
if (x == y) {
    console.log('2) Equals');
} else {
    console.log('2) Not equals');
}

x = 0;
y = false;
if (x == y) {
    console.log('3) Equals');
} else {
    console.log('3) Not equals');
}

// triple equals (===) does not do type conversion
// if comparable elements aren't of same time then they are not equal
if (x === y) {
    console.log('4) Equals');
} else {
    console.log('4) Not equals');
}

x = 1;
y = '1';
if (x === y) {
    console.log('5) Equals');
} else {
    console.log('5) Not equals');
}

if (x) {
    console.log('6) Exist');
} else {
    console.log('6) Not exist');
}

let z;
if (z) {
    console.log('7) Exist');
} else {
    console.log('7) Not exist');
}

// but below code is same as this
if (x == true) {
    console.log('8) Exist');
} else {
    console.log('8) Not exist');
}

// problem is if x = 0 it will be converted to false
x = 0;
console.log('x is', typeof x);
if (x) {
    console.log('9) Exist');
} else {
    console.log('9) Not exist');
}

x = '0';
console.log('x is', typeof x);
if (x) {
    console.log('10) Exist');
} else {
    console.log('10) Not exist');
}

console.log('z is', typeof z);
if (z) {
    console.log('11) Exist');
} else {
    console.log('11) Not exist');
}

// using if (a) will lead to is not defined error, because variable a is not declared
// this is how this error can be avoided
console.log('a is', typeof a);
if (typeof a !== 'undefined') {
    console.log('12) Exist');
} else {
    console.log('12) Not exist');
}

x = 0;
console.log('x is', typeof x);
// check for existence doesn't work
if (x) {
    console.log('13) Exist');
} else {
    console.log('13) Not exist');
}
// check for existence works
if (typeof x !== 'undefined') {
    console.log('14) Exist');
} else {
    console.log('14) Not exist');
}
