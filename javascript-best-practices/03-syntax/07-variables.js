// hoisting
console.log(myVariable);
var myVariable = 10;

// hoisting doesn't work with let and const
/*
 console.log(myVariable2);
let myVariable2;
*/

/*
console.log(myVariable3);
const myVariable3 = 10;
*/

// more problems with relying on hoisting
function func() {
    myVariable = 25;
}
console.log(myVariable);
func();
console.log(myVariable);

var myVariable1 = 10;
function func1() {
    myVariable1 = 25;

    var myVariable1;
}
console.log(myVariable1);
func1();
console.log(myVariable1);
