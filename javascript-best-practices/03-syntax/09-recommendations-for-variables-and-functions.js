// Recommendations for declaring and using variables and functions
// variables first...
var x = 10;
// functions next
function print(input) {
    // when creating functions, new scope is created, so same process applies inside function
    // variables first
    var x = 0; // this goes here...
    // functions next
    function log() {
        // log stuff
    }
    // run code
    console.log(input);
}
// run code
print(x);
