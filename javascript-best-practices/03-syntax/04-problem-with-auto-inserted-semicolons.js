/*
"When, a token is encountered that is allowed by some production of the grammar, but the
production is a restricted production and the token would be the first token of a restricted
production, and the restricted token is separated from the previous token by at least one
LineTerminator, then a semicolon is automatically inserted before the restricted token."
EcmaScript Standards

// Restricted production:
continue, break, return, or throw...
 */

// in the following code semicolon will be automatically inserted right after return => return;
function returnObject()
{
    if (true)
    {
        return
        {
            hi: 'hello'
        }
    }
}

// the following code will fix this problem
function returnObject2()
{
    if (true)
    {
        return {
            hi: 'hello'
        };
    }
}

/*
Good practice in using semicolons:
- Use semicolons in conjunction with JSHint (or ESLint) to prevent potential issues.
 */
