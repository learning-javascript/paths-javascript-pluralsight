
// this will work because of hoisting
myFunc();

// this will also be hoisted, but because it is variable (var),
// it will be initialised to undefined not a function
// expression();

var expression = function () {
    console.log('Hi from my expression');
};

function myFunc() {
    console.log('Hi from my func');
}

expression();
