// the following code have problem, because semicolon is automatically inserted right after return
function service()
{
    var get = function ()
    {
        console.log('get');
    }
    var set = function ()
    {
        console.log('set');
    }
    return
    {
        get: get,
        set: set
    }
}

// the following code solves the problem with automatic semicolon insertion, but
// it does look consistent since we have mixed formatting for curly braces
function service2()
{
    var get = function ()
    {
        console.log('get');
    }
    var set = function ()
    {
        console.log('set');
    }
    return {
        get: get,
            set: set
    }
}

// the following code looks more consistent
// this is general recommendation on how to place curly braces
function service3()  {
    var get = function () {
        console.log('get');
    };
    var set = function () {
        console.log('set');
    };
    return {
        get: get,
        set: set
    }
}
