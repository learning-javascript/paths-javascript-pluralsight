console.log(this);

function sayHello() {
    alert('Hello');
}

sayHello();
window.sayHello();

// this function overrides global alert function
function alert() {
    console.log('Hello!');
}
alert();