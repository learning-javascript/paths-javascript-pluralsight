const mark = {
    name: 'Mark',
    tshortColor: 'navi blue'
};

const lisa = {
    name: 'Lisa',
    tshortColor: 'red'
};

function Friend(name, tshortColor) {
    this.name = name;
    this.tshortColor = tshortColor;
}

const danny = new Friend('Danny', 'green');

console.log(mark);
console.log(lisa);
console.log(danny);
console.log(danny.name);
console.log(danny.tshortColor);
