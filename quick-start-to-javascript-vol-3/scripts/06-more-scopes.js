const three = '3';
function alertNumber() {
    const one = '1';
    alert(one);

    function alertAnotherNumber() {
        const two = '2';
        alert(two);
        alert(three);
        alert(one);
    }

    alertAnotherNumber();
}

alertNumber();

alert(one); // raise an error: one is not defined
