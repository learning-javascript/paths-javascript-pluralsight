// global variable
const example = 'value';
alert(example);

function alertExample() {
    alert('in function: ' + example);
    // local variable
    const anotherExample = "?";
}
alertExample();

alert(anotherExample); // raise an error: is not defined
