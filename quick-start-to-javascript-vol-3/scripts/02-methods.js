const myCoffee = {
    flavor: 'cappuccino',
    temperature: 'hot',
    ounses: 100,
    milk: true,

    reheat: function() {
        if (this.temperature === 'cold') {
            this.temperature = 'hot';
            alert('Your coffee has been reheated');
        }
    }
};

myCoffee.temperature = 'cold';
console.log(myCoffee);

myCoffee.reheat();
console.log(myCoffee);

myCoffee['temperature'] = 'warm';
myCoffee.reheat();