const coffeeFlavor = 'cappuccino';
const coffeeTemperature = 'hot';
const coffeeOunses = 100;
const coffeeMilk = true;

const myCoffee = {
    flavor: 'cappuccino',
    temperature: 'hot',
    ounses: 100,
    milk: true
};

console.log('my coffee', myCoffee);
console.log('flavor:', myCoffee.flavor);
console.log('temperature:', myCoffee.temperature);
console.log('ounses:', myCoffee.ounses);
console.log('milk:', myCoffee.milk);
