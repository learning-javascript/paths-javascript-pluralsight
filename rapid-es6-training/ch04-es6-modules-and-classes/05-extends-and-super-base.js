class Project {
    constructor() {
        console.log('constructing Project');
    }
}

class SoftwareProject extends Project {}

let p = new SoftwareProject();

class Project2 {
    constructor(name) {
        console.log('constructing Project 2:', name);
    }
}

class SoftwareProject2 extends Project2 {}

p = new SoftwareProject2('Mazatlan');

class SoftwareProject3 extends Project {
    constructor() {
        // if constructor doesn't have call to super() it will raise an error
        super();
        console.log('constructing SoftwareProject 3');
    }
}
p = new SoftwareProject3();

class Project4 {
}

class SoftwareProject4 extends Project4 {
    constructor() {
        // even though parent class doesn't have constructor, call to super() is mandatory
        super();
        console.log('constructing SoftwareProject 4');
    }
}
p = new SoftwareProject4();

class Project5 {
    getTaskCount() {
        return 50;
    }
}
class SoftwareProject5 extends Project5 {}
p = new SoftwareProject5();
console.log('1)', p.getTaskCount());

class SoftwareProject6 extends Project5 {
    getTaskCount() {
        return 66;
    }
}
p = new SoftwareProject6();
console.log('2)', p.getTaskCount());

class SoftwareProject7 extends Project5 {
    getTaskCount() {
        return super.getTaskCount() + 6;
    }
}
p = new SoftwareProject7();
console.log('3)', p.getTaskCount());

let project = {
    getTaskCount() { return 50; }
};
let softwareProject = {
    getTaskCount() {
        return super.getTaskCount() + 7;
    }
};
Object.setPrototypeOf(softwareProject, project);
console.log('4)', softwareProject.getTaskCount());
