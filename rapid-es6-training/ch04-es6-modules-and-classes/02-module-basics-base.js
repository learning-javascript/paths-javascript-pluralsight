console.log('1) in a base.js');

// next line will raise an error, since ES6 uses strict mode by default
// projectId = 99;

console.log('2) starting in base');
import { projectId } from '02-module-basics-module1.js';
console.log('3) module loaded in base');
console.log('4)', projectId);

import { projectId as id, projectName } from '02-module-basics-module2.js';
console.log(`5) ${projectName} has id: ${id}`);

import someValue from '02-module-basics-module3.js';
console.log('6)', someValue);

import {default as myProjectName} from '02-module-basics-module3.js';
console.log('7)', myProjectName);

import someValue1 from '02-module-basics-module2.js';
console.log('8)', someValue1);

import someValue2 from '02-module-basics-module4.js';
console.log('8)', someValue2);

import * as values from '02-module-basics-module5.js';
console.log('9)', values);
