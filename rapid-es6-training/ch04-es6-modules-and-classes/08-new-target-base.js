'use strict';

class Project {
    constructor() {
        console.log('1)', typeof new.target);
    }
}
let p = new Project();

class Project1 {
    constructor() {
        console.log('2)', new.target);
    }
}
p = new Project1();

class SoftwareProject extends Project1 {
    constructor() {
        super();
    }
}
p = new SoftwareProject();

class SoftwareProject1 extends Project1 {}
p = new SoftwareProject1();

class Project2 {
    constructor() {
        console.log('3)', new.target.getDefaultId());
    }
}
class SoftwareProject2 extends Project2 {
    static getDefaultId() { return 99; }
}
p = new SoftwareProject2();
