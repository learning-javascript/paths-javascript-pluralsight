class Task {
    // next line will raise a syntax error
    // let taskId = 9000;
    constructor() {
        console.log('constructing Task');
    }

    showId() {
        console.log('99');
    }
}

console.log('1)', typeof Task);

const task = new Task();
console.log('2)', typeof task);

console.log('3)', task instanceof Task);

task.showId();

console.log('4)', task.showId === Task.prototype.showId);

// classes are not hoisted, so they cannot be used before declaration

let newClass = class Task2 {
    constructor() {
        console.log('constructing Task 2');
    }
};
new newClass();

let Task3 = function () {
    console.log('constructing Task 3');
};
let task3 = {};
Task3.call(task3);

class Task4 {
    constructor() {
        console.log('constructing Task 4');
    }
}
let task4 = {};
Task4.call(task4);

function Project() {}
console.log('5)', window.Project === Project);

console.log('6)', window.Task === Task);
