class Project {
    static getDefaultId() {
        return 0;
    }
    // next line will cause a syntax error
    // static let id = 1;
}
console.log('1)', Project.getDefaultId());

let p = new Project();
// trying to access static member from instance will raise an error
// console.log(p.getDefaultId());

Project.id = 99;
console.log('2)', Project.id);
