'use strict';

import { projectId } from "./03-named-exports-in-modules-module1.js";
// next line raise an error: projectI is read-only
// projectId = 8000;
console.log('1)', projectId);

import { project, showProject } from "./03-named-exports-in-modules-module1.js";
project.projectId = 8000;
console.log('2)', project.projectId);

showProject();
console.log('3)', project.projectId);

import { showProject1, updateFunction } from "./03-named-exports-in-modules-module2.js";
showProject1();
updateFunction();
showProject1();