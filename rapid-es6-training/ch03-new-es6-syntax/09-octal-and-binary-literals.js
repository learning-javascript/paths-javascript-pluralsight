'use strict';

let value = 0o10;
console.log('1)', value);

value = 0O10;
console.log('2)', value);

value = 0b10;
console.log('3)', value);

value = 0B10;
console.log('4)', value);
