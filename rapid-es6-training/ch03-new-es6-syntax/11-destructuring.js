'use strict';

let salary = ['32000', '50000', '75000'];
let [low, average, high] = salary;
console.log('1)', average);

salary = ['32000', '50000'];
[low, average, high] = salary;
console.log('2)', high);

salary = ['32000', '50000', '75000'];
[low, , high] = salary;
console.log('3)', high);

let remaining;
[low, ...remaining] = salary;
console.log('4)',remaining);

salary = ['32000', '50000'];
[low, average, high = '88000'] = salary;
console.log('5)', high);

let actualLow, actualHigh;
salary = ['32000', '50000', ['88000', '99000']];
[low, average, [actualLow, actualHigh]] = salary;
console.log('6)', actualLow);

function reviewSalary([low, average], high = '88000') {
    console.log('7)', average);
}
reviewSalary(['32000', '50000']);

// destructuring objects
salary = {
    low1: '32000',
    average1: '50000',
    high1: '75000'
};
let { low1, average1, high1 } = salary;
console.log('8)', high1);

salary = {
    low: '32000',
    average: '50000',
    high: '75000'
};
let {low: newLow, average: newAverage, high: newHigh} = salary;
console.log('9)', newHigh);

// the below line raises an error
// {low: newLow, average: newAverage, high: newHigh} = salary;

// this can be fixed as following
({low: newLow, average: newAverage, high: newHigh} = salary);
console.log('10)', newHigh);

// destructuring string
let [maxCode, minCode] = 'AZ';
console.log(`11) min: ${minCode}, max: ${maxCode}`);
