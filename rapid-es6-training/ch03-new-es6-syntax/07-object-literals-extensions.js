'use strict';

const price = 5.99;
let quantity = 30;
let productView = {
    price,
    quantity
};
console.log('1)', productView);

quantity = 10;
productView = {
    price,
    quantity,
    calculateValue() {
        return this.price * this.quantity;
    }
};
console.log('2)', productView.calculateValue());

productView = {
    price: 7.99,
    quantity: 1,
    calculateValue() {
        return this.price * this.quantity;
    }
};
console.log('3)', productView.calculateValue());

productView = {
    price,
    quantity,
    'calculate value'() {
        return this.price * this.quantity;
    }
};
console.log('4)', productView['calculate value']());

const field = 'dynamicField';
productView = {
    [field]: price
};
console.log('5)', productView);

productView = {
    [field + '-001']: price
};
console.log('6)', productView);

const method = 'doIt';
productView = {
    [method + '-001']() {
        console.log('in a method');
    }
};
productView['doIt-001']();

const ident = 'productId';
productView = {
    get [ident] () { return true; },
    set [ident] (value) {}
};
console.log('7)', productView.productId);
