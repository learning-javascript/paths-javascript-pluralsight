'use strict';

const invoiceNum = '1350';
console.log(`1) Invoice number: ${invoiceNum}`);

console.log(`2) Invoice number: \${invoiceNum}`);

const message = `A
B
C`;
console.log(message);

console.log(`3) Invoice number: ${'INV-' + invoiceNum}`);

function showMessage(message) {
    const invoiceNum = '99';
    console.log(message);
}
showMessage(`4) Invoice number: ${invoiceNum}`);

function processInvoice(segments) {
    console.log('5)', segments);
}
processInvoice `template`;

function processInvoice2(segments, ...values) {
    console.log('6)', segments);
    console.log('6)', values);
}
const amount = '2000';
processInvoice2 `Invoice: ${invoiceNum} for ${amount}`;
