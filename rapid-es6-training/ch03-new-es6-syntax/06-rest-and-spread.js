'use strict';

const showCategories = function (productId, ...categories) {
    console.log('1)', categories instanceof Array);
};
showCategories(123, 'search', 'advertising');

const showCategories2 = function (productId, ...categories) {
    console.log('2)', categories);
};
showCategories2(123, 'search', 'advertising');

showCategories2(123);

const showCategories3 = function (productId, ...categories) {
};
console.log('3)', showCategories3.length);

const showCategories4 = function (productId, ...categories) {
    console.log('4)', arguments.length);
};
showCategories4(123, 'search', 'advertising');

const showCategories5 = new Function("...categories", "return categories");
console.log('5)', showCategories5('search', 'advertising'));

const prices = [12, 20, 18];
const maxPrice = Math.max(...prices);
console.log('6)', maxPrice);

let newPriceArray = [...prices];
console.log('7)', newPriceArray);

newPriceArray = Array(...[,,]);
console.log('8)', newPriceArray);

newPriceArray = [...[,,]];
console.log('9)', newPriceArray);

const maxCode = Math.max(..."43210");
console.log('10)', maxCode);

const codeArray = ["A", ..."BCD", "E"];
console.log('11)', codeArray);
