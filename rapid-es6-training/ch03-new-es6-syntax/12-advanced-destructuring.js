'use strict';

let [high, low] = [,];
console.log(`1) high: ${high}, low: ${low}`);

// the following line will raise a runtime error, destructuring requires an iterator
// [low, high] = undefined;

// the following line will raise exact same error
// [low, high] = null;

try {
    let [low, high] = undefined;
} catch (e) {
    console.log(e.name);
}

[ high, low, ] = [500, 200];
console.log(`2) high: ${high}, low: ${low}`);

for (let [a, b] of [[5, 10]]) {
    console.log(`3) ${a} ${b}`);
}

let count = 0;
for (let [a, b] of [[5, 10]]) {
    console.log(`4) ${a} ${b}`);
    count++;
}
console.log(`5) ${count}`);
