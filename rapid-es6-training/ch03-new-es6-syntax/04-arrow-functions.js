'use strict';

const getPrice = () => 5.99;
console.log('1)', typeof getPrice);

console.log('2)', getPrice());

const getPrice2 = count => count * 4.00;
console.log('3)', getPrice2(2));

const getPrice3 = (count, tax) => count * 4.00 * (1 + tax);
console.log('4)', getPrice3(2, .07));

const getPrice4 = (count, tax) => {
    let price = count * 4.00;
    price *= (1 + tax);
    return price;
};
console.log('5)', getPrice4(2, .07));

// the code below will print #document in the browser's console
/*
document.addEventListener('click', function () {
    console.log(this);
});
*/

// the code below will print global Window object
/*
document.addEventListener('click', () => console.log(this));
*/

const invoice1 = {
    number: 123,
    process: function() {
        console.log('6)', this);
    }
};
invoice1.process();

const invoice2 = {
    number: 123,
    process: () => console.log('7)', this)
};
invoice2.process();

const invoice3 = {
    number: 123,
    process: function () {
        return () => console.log('8)', this.number)
    }
};
invoice3.process()();

const newInvoice = {
    number: 456
};
// bind will be ignored
invoice3.process().bind(newInvoice)();

// we cannot change 'this' of arrow function with bind, call nor apply
invoice3.process().call(newInvoice);

// the following code raise an error, we cannot break arrow function declaration into multiple lines
/*
const getPrice5 = ()
    => 5.99;
*/

// arrow functions don't have access to prototype field
console.log('9)', getPrice.hasOwnProperty('prototype'));
