'use strict';
let productId = 12;
console.log('1)', productId);

let productId1;
console.log('2)', productId1); // this variable is undefined

{
    // this variable is visible only in this code block
    let productId = 2000;
}
// here it is still 12
console.log('3)', productId);

{
    let productId2 = 2000;
}
// next line will raise is not defined error
// console.log(productId2);

function updateProductId() {
    productId = 12;
}
productId = null;
updateProductId();
console.log('4)', productId);

productId = 42;
for (let productId = 0; productId < 10; productId++) {}
console.log('5)', productId);

// problem in ES5
let updateFunctions = [];
for (var i = 0; i < 2; i++) {
    updateFunctions.push(function () { return i; });
}
// var i is visible in global scope too
console.log('6)', updateFunctions[0]()); // prints: 2 which is unexpected

// same code in ES6
updateFunctions = [];
for (let i = 0; i < 2; i++) {
    updateFunctions.push(function () { return i; });
}
// var i is visible in global scope too
console.log('7)', updateFunctions[0]()); // prints: 0 which is expected

const MARKUP_PCT = 100;
console.log('8)', MARKUP_PCT);

// next line will raise an error, since constant should be initialized with value at declaration time
// const MARKUP_PCT1;

// next line will raise an error, constant cannot be re-assigned
// MARKUP_PCT = 10;

if (MARKUP_PCT > 0) {
   const  MARKUP_PCT = 10;
}
console.log('9)', MARKUP_PCT); // still 100
