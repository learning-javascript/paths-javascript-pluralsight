'use strict';

const getProduct = function (productId = 1000) {
    console.log('1)', productId);
};
getProduct();

const getProduct2 = function (productId = 1000, type = 'software') {
    console.log('2)', productId + ', ' + type);
};
getProduct2(undefined, 'hardware');

const getTotal = function (price, tax = price * 0.07) {
    console.log('3)', price + tax);
};
getTotal(5.00);

const baseTax = 0.07;
const getTotal2 = function (price, tax = price * baseTax) {
    console.log('4)', price + tax);
};
getTotal2(5.00);

const generateBaseTax = () => 0.07;
const getTotal3 = function (price, tax = price * generateBaseTax()) {
    console.log('5)', price + tax);
};
getTotal3(5.00);

const getTotal4 = function (price, tax = 0.07) {
    console.log('6)', arguments.length);
};
getTotal4(5.00);

const getTotal5 = function (price = adjustment, adjustment = 1) {
    console.log('7)', price + adjustment);
};
getTotal5(5.00); // this works
// getTotal5(); // defaults doesn't work here

const getTotal6 = new Function("price = 20.00", "return price");
console.log('8)', getTotal6());
