let buffer = new ArrayBuffer(1024);
let dv = new DataView(buffer);
console.log('1)', dv.byteLength);

dv = new DataView(buffer, 0, 32);
console.log('2)', dv.byteLength);

buffer = new ArrayBuffer(1024);
dv = new DataView(buffer);
dv.setUint8(0, 1);
console.log('3)', dv.getUint16(0));

console.log('4)', dv.getUint16(0, true));
