let perks = new Set();

perks.add('Car');
perks.add('Super Long Vacation');

console.log('1)', perks.size);

perks.add('Car');

console.log('2)', perks.size);

perks = new Set([
    'Car',
    '10 Weeks Vacation',
    'Jet'
]);
console.log('3)', perks.size);

let newPerks = new Set(perks);
console.log('4)', newPerks.size);

console.log('5)', perks.has('Jet'));
console.log('6)', perks.has('Cool Hat'));

perks = new Set(['Car', 'Jet']);
console.log('7)', ...perks.keys());
console.log('8)', ...perks.values());
console.log('9)', ...perks.entries());

perks = new Set([
    { id: 800 },
    { id: 800 }
]);
console.log('10)', perks.size);

perks = new Set([
    1,
    '1'
]);
console.log('11)', perks.size);

// next line will raise runtime error, because WeakSet operates on objects, not primitives
// perks = new WeakMap([1, 2, 3]);

let p1 = { name: 'Car' };
let p2 = { name: 'Jet' };
perks = new WeakSet([p1, p2]);
console.log('12)', perks.size);

console.log('13)', perks.has(p1));

p1 = null;
console.log('14)', perks.has(p1));
