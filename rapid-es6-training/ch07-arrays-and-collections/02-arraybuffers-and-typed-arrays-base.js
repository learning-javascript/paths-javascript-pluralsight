let buffer = new ArrayBuffer(1024);
console.log('1)', buffer.byteLength);

buffer[0] = 0xff;
console.log('2)', buffer[0]);

buffer = new ArrayBuffer(1024);
let a = new Int8Array(buffer);
a[0] = 0xff;
console.log('3)', a[0]);

buffer = new ArrayBuffer(1024);
a = new Uint8Array(buffer);
a[0] = 0xff;
console.log('4)', a[0]);

buffer = new ArrayBuffer(1024);
a = new Uint8ClampedArray(buffer);
a[0] = -12;
console.log('5)', a[0]);

buffer = new ArrayBuffer(1024);
a = new Uint8Array(buffer);
let b = new Uint16Array(buffer);
a[0] = 1;
console.log('6)', b[0]);

buffer = new ArrayBuffer(1024);
a = new Uint8Array(buffer);
b = new Uint16Array(buffer);
a[1] = 1;
console.log('7)', b[0]);
