let employee1 = { name: 'Jake' };
let employee2 = { name: 'Janet' };

let employees = new Map();
employees.set(employee1, 'ABC');
employees.set(employee2, '123');

console.log('1)', employees.get(employee1));

console.log('2)', employees.size);

employees.delete(employee2);
console.log('3)', employees.size);

employees = new Map();
employees.set(employee1, 'ABC');
employees.set(employee2, '123');

employees.clear();
console.log('4)', employees.size);

let arr = [
    [employee1, 'ABC'],
    [employee2, '123']
];
employees = new Map(arr);
console.log('5)', employees.size);

console.log('6)', employees.has(employee2));

let list = [...employees.values()];
console.log('7)', list);

list = [...employees.entries()];
console.log('7)', list[0][1]);

employees = new WeakMap([
    [employee1, 'ABC' ],
    [employee2, '123']
]);

employee1 = null;

console.log('8)', employees.size);
