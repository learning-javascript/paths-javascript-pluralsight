class Perks extends Array {}
let a = Perks.from([5, 10, 15]);

console.log('1)', a instanceof Perks);

console.log('2)', a.length);

let newArray = a.reverse();
console.log('3)', newArray instanceof Perks);

console.log('4)', newArray instanceof Array);

class Perks1 extends Array {
    sum() {
        let total = 0;
        this.map(v => total += v);
        return total;
    }
}
a = Perks1.from([5, 10, 15]);

console.log('5)', a.sum());
