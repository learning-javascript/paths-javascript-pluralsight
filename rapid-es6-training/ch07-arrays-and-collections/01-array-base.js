// Problem in ES5
let salaries = Array(90000);
console.log('1)', salaries.length);

// In ES6
salaries = Array.of(90000);
console.log('2)', salaries.length);

let amounts = [800, 810, 820];
salaries = Array.from(amounts, v => v + 100);
console.log('3)', salaries);

salaries = Array.from(amounts, function (v) {
    return v + this.adjustment;
}, { adjustment: 50 });
console.log('4)', salaries);

salaries = Array.from(amounts, v => v + this.adjustment,
    { adjustment: 50 });
console.log('5)', salaries);

salaries = [600, 700, 800];
salaries.fill(900);
console.log('6)', salaries);

salaries = [600, 700, 800];
salaries.fill(900, 1);
console.log('7)', salaries);

salaries = [600, 700, 800];
salaries.fill(900, 1, 2);
console.log('8)', salaries);

salaries = [600, 700, 800];
salaries.fill(900, -1);
console.log('9)', salaries);

salaries = [600, 700, 800];
let result = salaries.find(value => value >= 750);
console.log('10)', result);

result = salaries.find(value => value >= 650);
console.log('11)', result);

result = salaries.findIndex(function (value, index, array) {
    return value == this;
}, 700);
console.log('12)', result);

salaries.copyWithin(2, 0);
console.log('13)', salaries);

let ids = [1, 2, 3, 4, 5];
ids.copyWithin(0, 1);
console.log('14)', ids);

ids = [1, 2, 3, 4, 5];
ids.copyWithin(3, 0, 2);
console.log('15)', ids);

ids = ['A', 'B', 'C'];
console.log('16)', ...ids.entries());

console.log('17)', ...ids.keys());
