class Location {
    constructor() {
        console.log('1) constructing Location');
    }
}

class Restaurant extends Location {}

console.log('2)', Reflect.getPrototypeOf(Restaurant));

class Restaurant1 {}
let setup = {
    getId() { return 88;}
};

let r = new Restaurant1();
Reflect.setPrototypeOf(r, setup);
console.log('3)', r.getId());
