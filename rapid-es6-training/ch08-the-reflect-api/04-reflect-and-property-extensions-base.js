let rest = {
    id: 2000
};

rest.location = 'Goleta';

console.log('1)', rest.location);

rest = {
    id: 2000
};
console.log('3)', Reflect.isExtensible(rest));

Reflect.preventExtensions(rest);
rest.location = 'Goleta';
console.log('2)', rest.location);

console.log('4)', Reflect.isExtensible(rest));
