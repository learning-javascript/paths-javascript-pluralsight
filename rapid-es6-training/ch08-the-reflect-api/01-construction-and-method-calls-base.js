console.log('1)', typeof Reflect);

class Restaurant {}

let r = Reflect.construct(Restaurant, []);
console.log('2)', r instanceof Restaurant);

class Restaurant1 {
    constructor(name, city) {
        console.log(`${name} in ${city}`);
    }
}

r = Reflect.construct(Restaurant1, ["Zoye's", 'Goleta']);

class Restaurant2 {
    constructor() {
        console.log(`3) new target: ${new.target}`);
    }
}
function restaurantMaker() {
    console.log('4) in restaurantMaker');
}

r = Reflect.construct(Restaurant2, ["Zoye's", 'Goleta'], restaurantMaker);

class Restaurant3 {
    constructor() {
        this.id = 33;
    }
    show() {
        console.log('5)', this.id);
    }
}
Reflect.apply(Restaurant3.prototype.show, { id: 99 }, []);

class Restaurant4 {
    constructor() {
        this.id = 33;
    }
    show(prefix) {
        console.log('5) ' + prefix + this.id);
    }
}
Reflect.apply(Restaurant4.prototype.show, { id: 22 }, ['REST:']);
