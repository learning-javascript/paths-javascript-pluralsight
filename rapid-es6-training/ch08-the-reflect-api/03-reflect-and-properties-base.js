class Restaurant {
    constructor() {
        this.id = 8000;
    }
}

let r = new Restaurant();
console.log('1)', Reflect.get(r, 'id'));

class Restaurant1 {
    constructor() {
        this._id = 9000;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
}

r = new Restaurant1();
console.log('2)', Reflect.get(r, 'id', { _id: 88 }));

r = new Restaurant();
Reflect.set(r, 'id', 88);
console.log('3)', r.id);

r = new Restaurant1();
let alt = { id: 88 };
Reflect.set(r, '_id', 88, alt);
console.log('4)', r._id);
console.log('5)', alt._id);

class Location {
    constructor() {
        this.city = 'Goleta';
    }
}
class Restaurant2 extends Location {
    constructor() {
        super();
        this.id = 9000;
    }
}
r = new Restaurant2();
console.log('6)', Reflect.has(r, 'id'));
console.log('7)', Reflect.has(r, 'city'));

console.log('8)', Reflect.ownKeys(r));

class Restaurant3 {}
r = new Restaurant3();
Reflect.defineProperty(r, 'id', {
    value: 2000,
    configurable: true,
    enumerable: true
});
console.log('9)', r['id']);

let rest = {
    id: 2000
};
console.log('10)', rest.id);
Reflect.deleteProperty(rest, 'id');
console.log('11)', rest.id);

rest = {
    id: 2000
};
let d = Reflect.getOwnPropertyDescriptor(rest, 'id');
console.log('12)', d);
