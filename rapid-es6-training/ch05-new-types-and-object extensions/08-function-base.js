let fn = function calc() {
    return 0;
};
console.log('1)', fn.name);

fn = function() {
    return 0;
};
console.log('2)', fn.name);

let newFn = fn;
console.log('3)', newFn.name);

class Calculator {
    constructor() {}
    add() {}
}
let c = new Calculator();
console.log('4)', Calculator.name);
console.log('5)', c.add.name);
