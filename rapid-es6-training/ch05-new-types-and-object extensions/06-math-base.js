console.log('1)', Math.sign(0));
console.log('2)', Math.sign(-0));
console.log('3)', Math.sign(-20));
console.log('4)', Math.sign(20));
console.log('5)', Math.sign(NaN));

console.log('6)', Math.cbrt(27));

console.log('7)', Math.trunc(27.1));
console.log('8)', Math.trunc(-27.9));
