let eventSymbol = Symbol('resize event');
console.log('1)', typeof eventSymbol);

console.log('2)', eventSymbol.toString());

let CALCULATE_EVENT_SYMBOL = Symbol('calculate event');
console.log('3)', CALCULATE_EVENT_SYMBOL.toString());

let s = Symbol('event');
let s2 = Symbol('event');
console.log('4)', s === s2);

s = Symbol.for('event 1');
console.log('5)', s.toString());

s2 = Symbol.for('event 1');
console.log('6)', s === s2);

s2 = Symbol.for('event 2');
console.log('7)', s === s2);

s = Symbol.for('event');
let description = Symbol.keyFor(s);
console.log('8)', description);

let article = {
    title: 'Whiteface Mountain',
    [Symbol.for('article')]: 'My Article'
};
let value = article[Symbol.for('article')];
console.log('9)', value);

console.log('10)', Object.getOwnPropertyNames(article));

console.log('11)', Object.getOwnPropertySymbols(article));
