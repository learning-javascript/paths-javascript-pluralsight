console.log('1)', Number.parseInt === parseInt);

console.log('2)', Number.parseFloat === parseFloat);

// it is recommended to use Number functions, not global

let s = 'NaN';
// ES5 way
console.log('3)', isNaN(s));
// new ES6 function
console.log('4)', Number.isNaN(s));

s = '8000';
// ES5 way
console.log('5)', isFinite(s));
// new ES6 function
console.log('6)', Number.isFinite(s));

let sum = 408.2;
console.log('7)', Number.isInteger(sum));

console.log('8)', Number.isInteger(NaN));
console.log('9)', Number.isInteger(Infinity));
console.log('10)', Number.isInteger(undefined));
console.log('11)', Number.isInteger(3));

let a = Math.pow(2, 53) - 1;
console.log('12)', Number.isSafeInteger(a));
a = Math.pow(2, 53);
console.log('13)', Number.isSafeInteger(a));

console.log('14)', Number.EPSILON);
console.log('15)', Number.MAX_SAFE_INTEGER);
console.log('16)', Number.MIN_SAFE_INTEGER);
