let a = {
    x: 1
};
let b = {
    y: 2
};
Object.setPrototypeOf(a, b);
console.log('1)', a.y);

a = { a: 1 };
b = { b: 2 };
let target = {};
Object.assign(target, a, b);
console.log('2)', target);

b = { a: 5, b: 2 };
Object.assign(target, a, b);
console.log('3)', target);

Object.assign(target, b, a);
console.log('4)', target);

Object.defineProperty(b, 'c', {
    value: 10,
    enumerable: false
});
Object.assign(target, a, b);
console.log('5)', target);

b = { a: 5, b: 2 };
let c = { c: 20 };
Object.setPrototypeOf(b, c);
Object.assign(target, a, b);
// Object.assign will not go through prototype chain
console.log('6)', target);

let amount = NaN;
// ES5 code
console.log('7)', amount === amount);

// ES6 new function
console.log('8)', Object.is(amount, amount));

// ES5 code
amount = 0;
let total = -0;
console.log('9)', amount === total);

// ES6 new function
console.log('10)', Object.is(amount, total));

let article = {
    title: 'Whiteface Mountain',
    [Symbol.for('article')]: 'My Article'
};
console.log('11)', Object.getOwnPropertySymbols(article));
