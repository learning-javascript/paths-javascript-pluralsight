let pattern = /\u{1f3c4}/;
console.log('1)', pattern.test('🏄'));

pattern = /\u{1f3c4}/u;
console.log('2)', pattern.test('🏄'));

pattern = /^.Surfer/;
console.log('3)', pattern.test('🏄Surfer'));

pattern = /^.Surfer/u;
console.log('4)', pattern.test('🏄Surfer'));

pattern = /900/y;
console.log('5)', pattern.lastIndex);
console.log('6)', pattern.test('800900'));

pattern.lastIndex = 3;
console.log('7)', pattern.test('800900'));

pattern = /900/yg;
console.log('8)', pattern.flags);
