let title = 'Santa Barbara Surf Riders';
console.log('1)', title.startsWith('Santa'));

console.log('2)', title.endsWith('Rider'));
console.log('3)', title.endsWith('Riders'));

console.log('4)', title.includes('ba'));

title = "Surfer's \u{1f3c4} Blog";
console.log('5)', title);

let surfer = '\u{1f3c4}';
console.log('6)', surfer);
console.log('7)', surfer.length);

surfer = '\u{1f30a}\u{1f3c4}\u{1f40b}';
console.log('8)', Array.from(surfer).length);
console.log('9)', surfer);

title = 'Mazatla\u0301n';
// problem in ES5
console.log('10)', title + ' ' + title.length);

// fix in ES6
console.log('11)', title + ' ' + title.normalize().length);

console.log('12)', title.normalize().codePointAt(7).toString(16));

console.log('13)', String.fromCodePoint(0x1f3c4));

title = 'Surfer';
let output = String.raw`${title} \u{1f3c4}\n`;
console.log('14)', output);

let wave = '\u{1f30a}';
console.log('15)', wave.repeat(10));
