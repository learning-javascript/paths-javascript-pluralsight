let Blog = function () {};
let blog = new Blog();
console.log('1)', blog.toString());

Blog.prototype[Symbol.toStringTag] = 'Blog Class';
console.log('2)', blog.toString());

let values = [8, 12, 16];
console.log('3)', [].concat(values));

values[Symbol.isConcatSpreadable] = false;
console.log('4)', [].concat(values));

values = [8, 12, 16];
let sum = values + 100;
console.log('5)', sum);

values[Symbol.toPrimitive] = function (hint) {
    console.log('6)', hint);
    return 42;
};
sum = values + 100;
console.log('7)', sum);
