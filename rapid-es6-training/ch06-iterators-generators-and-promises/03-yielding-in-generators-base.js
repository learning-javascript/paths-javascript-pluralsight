function *process() {
    yield 8000;
}
let it = process();
console.log('1)', it.next());

function *process1() {
    yield;
}
it = process1();
console.log('2)', it.next());

function *process2() {
    let result = yield;
    console.log(`3) result is ${result}`);
}
it = process2();
it.next();
it.next(200);

it = process2();
it.next();
console.log('4)', it.next(200));

function *process3() {
    let newArray = [yield, yield, yield];
    console.log('5)', newArray[2]);
}
it = process3();
it.next();
it.next(2);
it.next(4);
it.next(42);

/* this code will cause syntax error
function *process4() {
    let value = 4 * yield 42;
    console.log(value);
}
*/

function *process4() {
    let value = 4 * (yield 42);
    console.log('6)', value);
}
it = process4();
it.next();
it.next(10);

function *process5() {
    yield 42;
    yield [1,2,3];
}
it = process5();
console.log('7)', it.next().value);
console.log('8)', it.next().value);
console.log('9)', it.next().value);

function *process6() {
    yield 42;
    yield* [1,2,3];
}
it = process6();
console.log('10)', it.next().value);
console.log('11)', it.next().value);
console.log('12)', it.next().value);
console.log('13)', it.next().value);
console.log('14)', it.next().value);
