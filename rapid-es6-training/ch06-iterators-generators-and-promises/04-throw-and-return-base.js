function *process() {
    try {
        yield 9000;
        yield 9001;
        yield 9002;
    } catch (e) {}
}
let it = process();
console.log('1)', it.next().value);
console.log('2)', it.throw('foo'));
console.log('3)', it.next());

function *process1() {
    yield 9000;
    yield 9001;
    yield 9002;
}
it = process1();
console.log('4)', it.next().value);
// next line will raise an exception
// it.throw('foo');

it = process1();
console.log('5)', it.next().value);
console.log('6)', it.return('foo'));
console.log('7)', it.next());
