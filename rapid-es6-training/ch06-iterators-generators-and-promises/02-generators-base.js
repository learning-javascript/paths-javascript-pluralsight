function *process() {
    yield 8000;
    yield 8001;
}
let it = process();
console.log('1)', it.next());

console.log('2)', it.next());

console.log('3)', it.next());

function *process1() {
    let nextId = 7000;
    while(true) {
        yield(nextId++);
    }
}
it = process1();
it.next();
console.log('4)', it.next().value);

for (let id of process1()) {
    if (id > 7002) break;
    console.log('5)', id);
}
