function doAsync() {
    let p = new Promise(function (resolve, reject) {
        console.log('!) in promise code');
        setTimeout(function () {
            console.log('2) resolving...');
            resolve('OK');
        }, 2000);
    });
    return p;
}
doAsync().then(function () {
    console.log('7) Fulfilled!');
}, function () {
    console.log('8) Rejected!');
});

function doAsync1() {
    let p = new Promise(function (resolve, reject) {
        console.log('3) in promise code');
        setTimeout(function () {
            console.log('4) rejecting...');
            reject('Database Error');
        }, 2000);
    });
    return p;
}
doAsync1().then(function () {
    console.log('5) Fulfilled!');
}, function () {
    console.log('6) Rejected!');
});

doAsync1().then(function (value) {
    console.log('9) Fulfilled!', value);
}, function (reason) {
    console.log('10) Rejected!', reason);
});

doAsync().then(function (value) {
    console.log('11) Fulfilled!', value);
}, function (reason) {
    console.log('12) Rejected!', reason);
});

doAsync().then(function (value) {
    console.log('13) Fulfilled!', value);
    return 'For Sure';
}).then(function (value) {
    console.log('14) Really!', value);
});

doAsync1().catch(function (reason) {
    console.log('15) Error:', reason);
});
