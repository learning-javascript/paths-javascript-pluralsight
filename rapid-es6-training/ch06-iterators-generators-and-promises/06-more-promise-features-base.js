function getAnotherRejectedPromise() {
    const p = new Promise(function (resolve, reject) {
        return reject();
    });
    return p;
}
function doAsync() {
    let p = new Promise(function (resolve, reject) {
        console.log('!) in promise code');
        setTimeout(function () {
            resolve(getAnotherRejectedPromise());
        }, 2000);
    });
    return p;
}
doAsync().then(function () { console.log('2) Ok'); },
               function () { console.log('3) Nope'); });

function doAsync1() {
    return Promise.resolve('Some String');
}
doAsync1().then(
    function (value) { console.log('4)', value); },
    function (reason) { console.log('5)', reason); }
);

function doAsync2() {
    return Promise.reject('Some Error');
}
doAsync2().then(
    function (value) { console.log('6)', value); },
    function (reason) { console.log('7)', reason); }
);

let p1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve('#1');
    }, 3000);
});
let p2 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve('#2');
    }, 5000);
});
Promise.all([p1, p2]).then(
    function (value) { console.log('8) Ok', value); },
    function () { console.log('9) Nope'); }
);

p1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve();
    }, 1000);
});
p2 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        reject();
    }, 2000);
});
Promise.all([p1, p2]).then(
    function () { console.log('10) Ok'); },
    function () { console.log('11) Nope'); }
);

p1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log('rejecting 1');
        reject();
    }, 3000);
});
p2 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log('rejecting 2');
        reject();
    }, 5000);
});
Promise.all([p1, p2]).then(
    function () { console.log('12) Ok'); },
    function () { console.log('13) Nope'); }
);

p1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log('resolving 1');
        resolve('#1');
    }, 3000);
});
p2 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log('resolving 2');
        resolve('#2');
    }, 5000);
});
Promise.race([p1, p2]).then(
    function (value) { console.log('14) Ok', value); },
    function () { console.log('15) Nope'); }
);

p1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        reject('#1');
    }, 3000);
});
p2 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve('#2');
    }, 5000);
});
Promise.race([p1, p2]).then(
    function (value) { console.log('16) Ok', value); },
    function (reason) { console.log('17) Nope', reason); }
);

p1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve('#1');
    }, 5000);
});
p2 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        reject('#2');
    }, 5000);
});
Promise.race([p1, p2]).then(
    function (value) { console.log('18) Ok', value); },
    function (reason) { console.log('19) Nope', reason); }
);
