let ids = [9000,9001, 9002];
console.log('1)', typeof ids[Symbol.iterator]);

let it = ids[Symbol.iterator]();
console.log('2)', it.next());

it.next();
console.log('3)', it.next());

console.log('4)', it.next()); // iterator already exhausted here
console.log('5)', it.next()); // subsequent calls return same object

let idMaker = {
    [Symbol.iterator]() {
        let nextId = 8000;
        return {
            next() {
                return {
                    value: nextId++,
                    done: false
                };
            }
        };
    }
};
it = idMaker[Symbol.iterator]();
console.log('6)', it.next().value);
console.log('7)', it.next().value);

for (let v of idMaker) {
    if (v > 8002) break;
    console.log('8)', v);
}

idMaker = {
    [Symbol.iterator]() {
        let nextId = 8000;
        return {
            next() {
                let value = nextId > 8002 ? undefined : nextId++;
                let done = !value;
                return { value, done };
            }
        };
    }
};
for (let v of idMaker) {
    console.log('9)', v);
}

ids = [8000,8001, 8002];
function process(id1, id2, id3) {
    console.log('10)', id3);
}
process(...ids);
